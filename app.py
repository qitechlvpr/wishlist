from flask import Flask
from flask_restful import Resource, Api
from Resources.wish import Wish, Wishing

app = Flask(__name__)
api = Api(app)

api.add_resource(Wish, '/wishes')
api.add_resource(Wishing, '/wishes/<string:id>')

if __name__ == '__main__':
    app.run(debug=True)