from flask_restful import Resource, reqparse
from models.wish import WishModel 

wishes = [
    {
        'nome_id': 'lui',
        'profissional': 'lui',
        'pessoal': 'lui',
        'viagem': 'lui'
    },
    {
        'nome_id': 'giu',
        'profissional': 'giu',
        'pessoal': 'giu',
        'viagem': 'giu'
    },
    {
        'nome_id': 'executivo',
        'profissional': 'Qitech, ASAS',
        'pessoal': 'academia de seg a sexta',
        'viagem': 'chapada'
    },
    {
        'nome_id': 'pleno',
        'profissional': 'crescer na QiTech, ASAS com todos os projetos aprovados',
        'pessoal': 'morar na praia',
        'viagem': 'Alemanhã'
    },
    {
        'nome_id': 'casamento',
        'profissional': 'responsavel por um setor na QiTech, ASAS referência nacional de trabalho social',
        'pessoal': 'casamento em Cumuruxatiba',
        'viagem': 'polo norte aurora boreal'
    },
    {
        'nome_id': 'senhor',
        'profissional': 'presidente na QiTech, modelo ASAS sendo utilizado no Brasil',
        'pessoal': 'projeto posso ajudar e banco de dados ambientais implementados',
        'viagem': 'volta ao mundo'
    },
    {
        'nome_id': 'teste1',
        'profissional': 'dono e investidor em uma rede de tecnologia',
        'pessoal': 'projetos ambientais pelo mundo',
        'viagem': 'passar uma semana em um iglu'
    },
    {
        'nome_id': 'teste2',
        'profissional': 'tecnologia ambiental aplicada na rede',
        'pessoal': 'transcender o corpo e espírito',
        'viagem': 'safari na África'
    },
    {
        'nome_id': 'teste3',
        'profissional': 'fundar uma empresa de tecnologia ambiental - sustentar terra',
        'pessoal': 'a gestão de terrras do Brasil e o clima mundial - projeto',
        'viagem': 'resort 6 estrelas (África)'
    },
    {
        'nome_id': 'teste4',
        'profissional': 'Sustentar Terra na bolsa',
        'pessoal': 'rede de aopio aos índios e à diversidade ambiental do Brasil',
        'viagem': 'xingu'
    }
]

class Wish(Resource):
    def get(self):
        return {'wish': wishes}

class Wishing(Resource):
    argumentos = reqparse.RequestParser()
    argumentos.add_argument('profissional')
    argumentos.add_argument('pessoal')
    argumentos.add_argument('viagem')


    def find_wish(nome_id):
        for wishing in wishes:
            if wishing['nome_id'] == nome_id:
                return wishing
        return None

    def get(self, nome_id):
        wished = Wishing.find_wish(nome_id)
        if wished:
            return wished                
        return {'message': 'Wish not found =('}, 404

    def post(self, nome_id):
        dados = Wishing.argumentos.parse_args()
        obj_wish = WishModel(nome_id, **dados)
        novo_wish = obj_wish.json()
        wishes.append(novo_wish)
        return novo_wish, 201

    def put(self, nome_id):
        dados = Wishing.argumentos.parse_args()
        obj_wish = WishModel(nome_id, **dados)
        novo_wish = obj_wish.json()
        wished = Wishing.find_wish(nome_id)
        if wished:
            wished.update(novo_wish)
            return novo_wish, 200
        wishes.append(novo_wish)
        return novo_wish, 201

    def delete(self, nome_id):
        global wishes
        wishes = [wished for wished in wishes if wished['nome_id'] != nome_id]
        return {'message': 'wish deleted.'}


class Wishes(Resource):
    atributos = reqparse.RequestParser()
    atributos.add_argument('profissional')
    atributos.add_argument('pessoal')
    atributos.add_argument('viagem')
