
class WishModel:
    def __init__(self, nome_id, profissional, pessoal, viagem):
        self.nome_id = nome_id
        self.profissional = profissional
        self.pessoal = pessoal
        self.viagem = viagem
    
    def json(self):
        return{
            'nome_id': self.nome_id,
            'profissional': self.profissional,
            'pessoal': self.pessoal,
            'viagem': self.viagem
        }