from flask import Flask
from flask_restful import Api
from resources.wish import Wish, Wishing 

app = Flask(__name__)
api = Api(app)

api.add_resource(Wish, '/wish')
api.add_resource(Wishing, '/wishes/<string:nome_id>')

if __name__ =='__main__':
    app.run(debug=True)

